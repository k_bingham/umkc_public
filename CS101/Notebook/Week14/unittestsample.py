import unittest

class ExampleUnitTest(unittest.TestCase):
    
    def test_CalculateLineTotalReturnsTotal(self):
        returnVal = CalculateLineTotal(1, 10, 0.05)
        self.assertEqual(returnVal, 10.5, "The Total calculated incorrectly.")
        
    def test_CalculateLineTotalReturnsZeroForNoQuantity(self):
        returnVal = CalculateLineTotal(0, 10, 0.10)
        self.assertEqual(returnVal, 0, "Zero Quantity should have no line total")


def CalculateLineTotal(qty : int, price : float, tax_rate : float):
    """
    >>> CalculateLineTotal(1, 10, 0.05)
    10.5
    """
    line_total = qty * price
    return line_total + line_total * tax_rate


unittest.main()
