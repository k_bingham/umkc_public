import csv
import os

output_lst = []
output_file = open("schedule.txt", "w")

for file in os.listdir():
    filenamebase, ext = os.path.splitext(file)
    
    if ext.lower() == ".csv":
        input_file = open(file, "r")
        input_reader = csv.reader(input_file)

        for row in input_reader:
            if row[0].upper() != "START_DATE":
                out_str = "{},{},{},{}".format( filenamebase, row[0], row[1], row[4])
                #print(out_str)
                #output_lst.append(out_str)
                print(out_str, file=output_file)

        input_file.close()

#output_file.writelines(output_lst)
output_file.close()
        
